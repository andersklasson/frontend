import { DefaultAction } from "./actions";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import rootReducer, { AppState } from "./reducers";

export default function configureStore() {
  return createStore<AppState, any, any, any>(
    rootReducer,
    applyMiddleware(thunk)
  );
}
