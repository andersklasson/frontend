import { Employment } from "../models/employmentModel";

export interface DefaultAction {
  type: string;
  payload?: any;
}

export const SET_EMPLOYMENTS = "SET_EMPLOYMENTS";
export const REMOVE_EMPLOYMENT = "REMOVE_EMPLOYMENT";

export type EmploymentActionType =
  | typeof SET_EMPLOYMENTS
  | typeof REMOVE_EMPLOYMENT;

export interface EmploymentsAction extends DefaultAction {
  type: EmploymentActionType;
  payload: Employment[];
}
