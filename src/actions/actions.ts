import { Employment } from "../models";
import Axios from "axios";
import {
  EmploymentsAction,
  SET_EMPLOYMENTS,
  REMOVE_EMPLOYMENT,
} from "./actionModel";

const MVC_API = Axios.create({
  baseURL: "http://localhost:5000/api/",
});

export function setEmployments(employments: Employment[]): EmploymentsAction {
  return {
    type: SET_EMPLOYMENTS,
    payload: employments,
  };
}

function removeEmployment(employment: Employment): EmploymentsAction {
  return {
    type: REMOVE_EMPLOYMENT,
    payload: [employment],
  };
}

export function postEmployment(e: Employment) {
  return async (dispatch: any) => {
    const res = await MVC_API.post("employments", e);

    if (res.data.remove) {
      dispatch(removeEmployment(e));
    }
  };
}
