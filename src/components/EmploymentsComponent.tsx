import React, { Component } from "react";
import { connect } from "react-redux";

import Container from "react-bootstrap/Container";
import { Employment } from "../models";
import { AppState } from "../reducers";
import { postEmployment } from "../actions";
import {
  EmploymentsListViewSM,
  EmploymentsListViewXL,
} from "./EmploiymentsListViews";

class EmploymentsComponent extends Component<EmploymentsComponentProps, {}> {
  render() {
    return (
      <Container fluid className="sectionContainer">
        <h1>Anställningar</h1>

        <div className="largeScreenOnly">
          <EmploymentsListViewXL {...this.props} />
        </div>

        <div className="smallScreenOnly">
          <EmploymentsListViewSM {...this.props} />
        </div>
      </Container>
    );
  }
}

interface EmploymentsComponentProps {
  employments: Employment[];
  postEmployment: (e: Employment) => void;
}

function mapStateToProps(state: AppState) {
  return {
    employments: state.employments,
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    postEmployment: (e: Employment) => dispatch(postEmployment(e)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmploymentsComponent);
