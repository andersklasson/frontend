import React from "react";
import Table from "react-bootstrap/Table";
import Figure from "react-bootstrap/Figure";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Employment } from "../models";

interface EmploymentListViewProps {
  employments: Employment[];
  postEmployment: (e: Employment) => void;
}

export function EmploymentsListViewXL(props: EmploymentListViewProps) {
  const { employments, postEmployment } = props;
  return (
    <Table striped={true} bordered={true} hover={true}>
      <thead>
        <tr>
          <th>Employment Id</th>
          <th>Name</th>
          <th>Country</th>
          <th>Age</th>
          <th>Salary</th>
          <th>Active</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {employments.map((employment) => XLRow({ employment, postEmployment }))}
      </tbody>
    </Table>
  );
}

export function EmploymentsListViewSM(props: EmploymentListViewProps) {
  const { employments, postEmployment } = props;
  return (
    <div>
      {employments.map((employment) => SMRow({ employment, postEmployment }))}
    </div>
  );
}

function XLRow(props: {
  employment: Employment;
  postEmployment: (e: Employment) => void;
}) {
  const { employment, postEmployment } = props;

  const {
    ActiveEmployment,

    Country,
    EmploymentId,
  } = employment;
  const formatted = formatEmployment(employment);
  return (
    <tr key={EmploymentId.toString()}>
      <td>{formatted.EmploymentId}</td>
      <td>{formatted.Name}</td>
      <td>
        <Figure>
          <Figure.Image
            fluid={false}
            width={60}
            height={40}
            src={`Flaggor/${Country}.jpg`}
          />
          <Figure.Caption>{Country}</Figure.Caption>
        </Figure>
      </td>
      <td>{formatted.Age}</td>
      <td>{formatted.Salary}</td>
      <td>{formatted.ActiveEmployment}</td>
      <td>
        <Button
          disabled={!ActiveEmployment}
          onClick={() => postEmployment(employment)}
        >
          POST
        </Button>
      </td>
    </tr>
  );
}

function SMRow({
  employment,
  postEmployment,
}: {
  employment: Employment;
  postEmployment: (e: Employment) => void;
}) {
  const { ActiveEmployment, EmploymentId, Country } = employment;

  const formatted = formatEmployment(employment);

  return (
    <Card.Body key={EmploymentId.toString()} className={"cardListStriped"}>
      <Row>
        <Col>
          <strong>Employment Id</strong> {formatted.EmploymentId} <br />
          <strong>Name</strong> {formatted.Name} <br />
          <strong>Age</strong> {formatted.Age} <br />
          <strong>Salary</strong> {formatted.Age} <br />
          <strong>Active</strong> {formatted.ActiveEmployment} <br />
        </Col>
        <Col>
          <Figure>
            <Figure.Image
              fluid={false}
              width={105}
              height={70}
              src={`Flaggor/${Country}.jpg`}
            />
            <Figure.Caption>{formatted.Country}</Figure.Caption>
          </Figure>
        </Col>
      </Row>

      <Button
        style={{ width: "100%", marginTop: "0.5em" }}
        disabled={!ActiveEmployment}
        onClick={() => postEmployment(employment)}
      >
        POST
      </Button>
    </Card.Body>
  );
}

function formatEmployment(e: Employment) {
  const { ActiveEmployment, Salary, ...keep_raw_values } = e;
  return {
    ActiveEmployment: ActiveEmployment ? "Yes" : "No",
    Salary: Salary.toFixed(2),
    ...keep_raw_values,
  };
}
