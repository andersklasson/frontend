import {
  EmploymentsAction,
  REMOVE_EMPLOYMENT,
  SET_EMPLOYMENTS,
} from "../actions";

import { Employment } from "../models/employmentModel";

export default function newProperty(
  state: EmploymentsState = [],
  { payload, type }: EmploymentsAction
): EmploymentsState {
  switch (type) {
    case REMOVE_EMPLOYMENT:
      const [to_remove] = payload;
      return [...state].filter(
        (e) => e.EmploymentId !== to_remove.EmploymentId
      );
    case SET_EMPLOYMENTS:
      return payload;
    default:
      return state;
  }
}

export type EmploymentsState = Employment[];
