import { combineReducers } from "redux";

import employments, { EmploymentsState } from "./employmentsReducer";

const rootReducer = combineReducers({
  employments,
});

export interface AppState {
  employments: EmploymentsState;
}

export default rootReducer;
