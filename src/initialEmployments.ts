import { Employment } from "./models";

export const INITIAL_EMPLOYMENTS: Employment[] = [
  {
    EmploymentId: 2301,
    Name: "Anders Johansson",
    Country: "Sweden",
    Age: 34,
    Salary: 21000.45,
    ActiveEmployment: false,
  },
  {
    EmploymentId: 8745,
    Name: "Mikael Svensson",
    Country: "Norway",
    Age: 22,
    Salary: 4500,
    ActiveEmployment: true,
  },
  {
    EmploymentId: 3456,
    Name: "Dennis Jansson",
    Country: "Denmark",
    Age: 43,
    Salary: 54000.5,
    ActiveEmployment: true,
  },
  {
    EmploymentId: 7847,
    Name: "Ove Larsson",
    Country: "Sweden",
    Age: 32,
    Salary: 33000,
    ActiveEmployment: false,
  },
  {
    EmploymentId: 9784,
    Name: "Kenneth Karlsson",
    Country: "Finland",
    Age: 65,
    Salary: 25300,
    ActiveEmployment: true,
  },
];
