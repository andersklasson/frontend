import "bootstrap/dist/css/bootstrap.min.css";
import "./css/globalCss.css";
import React from "react";

import "./App.css";
import Employments from "./components/EmploymentsComponent";

function App() {
  return (
    <div id="employmentsArea">
      <img src="Logo/SpLogo.png" />
      <Employments />
    </div>
  );
}

export default App;
