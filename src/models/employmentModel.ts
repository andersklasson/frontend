export interface Employment {
  EmploymentId: number;
  Name: string;
  Country: string;
  Age: number;
  Salary: number;
  ActiveEmployment: boolean;
}
